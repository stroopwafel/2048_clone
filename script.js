tile_positions=[];
tiles=0;
function addTile(){
    n=Math.round(Math.random())*2+2;
    x=Math.round(Math.random()*3)*110;
    y=Math.round(Math.random()*3)*110;

    // if(tile_positions.length==16){
    //     alert('vol');
    //     return;
    // }

    while(tile_positions.indexOf(x+','+y)!=-1){
        x=Math.round(Math.random()*3)*110;
        y=Math.round(Math.random()*3)*110;
    }

    tile_positions.push(x+','+y);

    $('.grid').append('<div id="block'+tiles+'" class="cell" style="left: '+x+'px; top: '+y+'px">'+n+'</div>');
    tiles++;
    // alert(tile_positions);
}
$(document).ready(function(){
    for(i=0;i<2;i++){
        addTile();
    }
});

$(document).keydown(function(e){
    switch(e.which){
        case 37: //left
        $('.cell').animate({left: 0}, 250);
        for(i=0;i<tiles;i++){
            tile_positions[i]=parseInt($('#block'+i).css('left'))+','+parseInt($('#block'+i).css('top'));
        }
        addTile();
        break;

        case 38: //up
        $('.cell').animate({top: 0}, 250);
        for(i=0;i<tiles;i++){
            tile_positions[i]=parseInt($('#block'+i).css('left'))+','+parseInt($('#block'+i).css('top'));
        }
        addTile();
        break;

        case 39: //right
        $('.cell').animate({left: 330}, 250);
        for(i=0;i<tiles;i++){
            tile_positions[i]=parseInt($('#block'+i).css('left'))+','+parseInt($('#block'+i).css('top'));
        }
        addTile();
        break;

        case 40: //down
        $('.cell').animate({top: 330}, 250);
        for(i=0;i<tiles;i++){
            tile_positions[i]=parseInt($('#block'+i).css('left'))+','+parseInt($('#block'+i).css('top'));
        }
        addTile();
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});
